#!/usr/bin/python -O
import sys


def generateRandomArray(array_length):
    import random
    range_array = range(0,array_length)
    random.shuffle(range_array)
    return range_array

def parseArgs():
    array_length = int(sys.argv[1])
    return array_length

def main():
    array_length = parseArgs()
    output_file = open("inputfile.csv",'w')
    random_array = generateRandomArray(array_length)
    for i in random_array:
        output_file.write("%d,"%(i))
    output_file.close()



if __name__ == '__main__':
    main()
