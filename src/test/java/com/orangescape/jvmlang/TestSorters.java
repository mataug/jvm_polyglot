package com.orangescape.jvmlang;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestSorters  {
	Sorter pythonSorter,javaSorter,rubySorter,scalaSorter;
	
	public static boolean ArrayEquals(int[] expected,int[] actual){
		if(expected.length != actual.length) return false;

		for(int i=0;i<expected.length;i++ )
			if(expected[i]!=actual[i]) return false;

		return true;
	}
	@Before
	public void setUp() throws Exception {
		
		 pythonSorter = SortFactory.createPython();
		 javaSorter = SortFactory.createJava();
		 rubySorter = SortFactory.createRuby();
		 scalaSorter = SortFactory.createScala();
	}

	@Test
	public void testCanary() {
		assertTrue(true);
	}
	@Test public void testArrayEquals(){
		int[] actual = {1,2,3};
		int[] expected = {1,2,3};
		
		assertTrue(ArrayEquals(actual, expected));
		
		int[] invalid = {3,2,1}; 
		assertFalse(ArrayEquals(invalid, actual));
	}
	@Test
	public void testPythonSorter(){
		int[] unsorted = { 3,2,1 };
		int[] sorted = {1,2,3};
		assertTrue(ArrayEquals(sorted, pythonSorter.sort(unsorted)));
	}
	@Test
	public void testJavaSorter(){
		int[] unsorted = { 3,2,1 };
		int[] sorted = { 1,2,3 };
		assertTrue(ArrayEquals(sorted, javaSorter.sort(unsorted)));
	}
	
	@Test
	public void testRubySorter(){
		int[] unsorted = { 3,2,1 };
		int[] sorted = { 1,2,3 };
		assertTrue(ArrayEquals(sorted, rubySorter.sort(unsorted)));
	}
	@Test
	public void testScalaSorter(){
		int[] unsorted = { 3,2,1 };
		int[] sorted = { 1,2,3 };
		assertTrue(ArrayEquals(sorted, scalaSorter.sort(unsorted)));
	}

}
