package com.orangescape.jvmlang.scala;
import com.orangescape.jvmlang.Sorter

class HeapSort extends Sorter{
  def sort(arr: Array[Int]): Array[Int] = {
    heapSort(arr)
  }
  private def heapSort(arr: Array[Int]): Array[Int] = {
    buildHeap(arr)
    (arr.length - 1 until 0 by -1).foreach(i => {
      swap(arr, 0, i)
      heapify(arr, 0, i)
    })
    arr
  }

  def buildHeap(arr: Array[Int]) {
    ((arr.length / 2.0D).floor.toInt - 1 until -1 by -1).foreach(i => heapify(arr, i, arr.length))
  }

  def heapify(arr: Array[Int], idx: Int, max: Int) {
    val l = left(idx)
    val r = right(idx)
    var largest = if (l < max && arr(l) > arr(idx)) l else idx
    largest = if (r < max && arr(r) > arr(largest)) r else largest
    if (largest != idx) {
      swap(arr, idx, largest)
      heapify(arr, largest, max)
    }
  }

  private def parent(idx: Int): Int = (idx / 2.0D).floor.toInt
  private def left(idx: Int): Int = 2 * idx + 1
  private def right(idx: Int): Int = (2 * idx) + 2

  def swap(s: Array[Int], i: Int, j: Int): Unit = {
    val v = s(i);
    s(i) = s(j);
    s(j) = v
  }

}

