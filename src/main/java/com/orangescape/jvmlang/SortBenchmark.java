package com.orangescape.jvmlang;

import java.io.IOException;

import com.google.caliper.Param;
import com.google.caliper.SimpleBenchmark;
import com.orangescape.jvmlang.ruby.HeapSort;

import csv.Parser;

public class SortBenchmark extends SimpleBenchmark{
	@Param({"10", "100", "1000", "10000"}) private int length;

	int[] unsorted ;
	Sorter pythonSorter=SortFactory.createPython()
			,javaSorter=SortFactory.createJava()
			,rubySorter = SortFactory.createRuby()
			,scalaSorter = SortFactory.createScala()
			,jsSorter = SortFactory.createJS()
			,groovySorter  = SortFactory.creategroovy()

			;
	
	public SortBenchmark() throws IOException {
		unsorted = Parser.unbox(Parser.parseIntCsvFile(System.getProperty("inputfile", "inputfile.csv")));
	}
	
/*
 * There is no way to specify type in JS
 * Unless explicitly used as JavaObjects 
 * instead of JS objects	
 * 
 * Untestable by the framework 
 * public void timeJsSorting(int reps){
		for(int i = 0 ; i< reps ; i++){
			jsSorter.sort(unsorted);
		}
	}
*/
	
	public void timeScalaSort(int reps){
		for(int i = 0 ; i< reps ; i++){
			scalaSorter.sort(unsorted);
		}
	}
	public void timeGroovySort(int reps){
		for(int i = 0 ; i< reps ; i++){
			groovySorter.sort(unsorted);
		}
	}
	
	public void timeRubyHeapSort(int reps){
		for(int i = 0 ; i< reps ; i++){
			rubySorter.sort(unsorted);
		}
	}
	
	public void timePythonSorting(int reps){
		for(int i = 0 ; i< reps ; i++){
			pythonSorter.sort(unsorted);
		}
	}
	
	public void timeJavaSorting(int reps){
		for(int i = 0 ; i< reps ; i++){
			javaSorter.sort(unsorted);
		}
	}
	
	
	
	

}
