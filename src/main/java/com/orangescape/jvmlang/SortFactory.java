package com.orangescape.jvmlang;

import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import com.orangescape.jvmlang.java.JavaHeapSort;



public class SortFactory {

	public static Sorter createPython(){
		PyObject heapSortClass;

		PythonInterpreter interpreter = new PythonInterpreter();
		interpreter.exec("from heap_sort import HeapSort");
		
		heapSortClass = interpreter.get("HeapSort");
		
		PyObject heapSortObject = heapSortClass.__call__();
		return (Sorter) heapSortObject.__tojava__(Sorter.class);
	}
	
	public static Sorter createJava(){
		return new JavaHeapSort();
	}
	
	public static Sorter createRuby(){
		return new com.orangescape.jvmlang.ruby.HeapSort();
	}
	public static Sorter createScala(){
		return new com.orangescape.jvmlang.scala.HeapSort();
	}
	
	public static Sorter createJS(){
		return new com.orangescape.jvmlang.js.HeapSort();
	}
	public static Sorter creategroovy(){
		return new com.orangescape.jvmlang.groovy.HeapSort();
	}
}
