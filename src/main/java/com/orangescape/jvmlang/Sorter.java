package com.orangescape.jvmlang;

public interface Sorter {
	public int[] sort(int[] unsorted);
}
