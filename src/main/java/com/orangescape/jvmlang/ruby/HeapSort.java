package com.orangescape.jvmlang.ruby;

import org.jruby.Ruby;
import org.jruby.RubyObject;
import org.jruby.javasupport.util.RuntimeHelpers;
import org.jruby.runtime.builtin.IRubyObject;
import org.jruby.javasupport.JavaUtil;
import org.jruby.RubyClass;
import com.orangescape.jvmlang.Sorter;


public class HeapSort extends RubyObject implements Sorter {
    private static final Ruby __ruby__ = Ruby.getGlobalRuntime();
    private static final RubyClass __metaclass__;

    static {
        String source = new StringBuilder("require 'java'\n" +
            "java_import 'com.orangescape.jvmlang.Sorter'\n" +
            "java_package 'com.orangescape.jvmlang.ruby'\n" +
            "\n" +
            "\n" +
            "class HeapSort \n" +
            "  java_implements 'Sorter'\n" +
            "  java_signature 'public  int[] sort(int[] array)'\n" +
            "  def sort(array)\n" +
            "    # in pseudo-code, heapify only called once, so inline it here\n" +
            "    ((array.length - 2) / 2).downto(0) {|start| siftdown(start, array.length - 1, array)}\n" +
            " \n" +
            "    # \"end\" is a ruby keyword\n" +
            "    (array.length - 1).downto(1) do |end_|\n" +
            "      array[end_], array[0] = array[0], array[end_]\n" +
            "      siftdown(0, end_ - 1, array)\n" +
            "    end\n" +
            "    array\n" +
            "  end\n" +
            "  java_signature 'public void siftdown(int start,int end_,int[] array)'\n" +
            "  def siftdown(start, end_, array)\n" +
            "    root = start\n" +
            "    loop do\n" +
            "      child = root * 2 + 1\n" +
            "      break if child > end_\n" +
            "      if child + 1 <= end_ and array[child] < array[child + 1]\n" +
            "        child += 1\n" +
            "      end\n" +
            "      if array[root] < array[child]\n" +
            "        array[root], array[child] = array[child], array[root]\n" +
            "        root = child\n" +
            "      else\n" +
            "        break\n" +
            "      end\n" +
            "    end\n" +
            "  end\n" +
            "end\n" +
            "\n" +
            "\n" +
            "").toString();
        __ruby__.executeScript(source, "HeapSort.rb");
        RubyClass metaclass = __ruby__.getClass("HeapSort");
        metaclass.setRubyStaticAllocator(HeapSort.class);
        if (metaclass == null) throw new NoClassDefFoundError("Could not load Ruby class: HeapSort");
        __metaclass__ = metaclass;
    }

    /**
     * Standard Ruby object constructor, for construction-from-Ruby purposes.
     * Generally not for user consumption.
     *
     * @param ruby The JRuby instance this object will belong to
     * @param metaclass The RubyClass representing the Ruby class of this object
     */
    private HeapSort(Ruby ruby, RubyClass metaclass) {
        super(ruby, metaclass);
    }

    /**
     * A static method used by JRuby for allocating instances of this object
     * from Ruby. Generally not for user comsumption.
     *
     * @param ruby The JRuby instance this object will belong to
     * @param metaclass The RubyClass representing the Ruby class of this object
     */
    public static IRubyObject __allocate__(Ruby ruby, RubyClass metaClass) {
        return new HeapSort(ruby, metaClass);
    }
        
    /**
     * Default constructor. Invokes this(Ruby, RubyClass) with the classloader-static
     * Ruby and RubyClass instances assocated with this class, and then invokes the
     * no-argument 'initialize' method in Ruby.
     *
     * @param ruby The JRuby instance this object will belong to
     * @param metaclass The RubyClass representing the Ruby class of this object
     */
    public HeapSort() {
        this(__ruby__, __metaclass__);
        RuntimeHelpers.invoke(__ruby__.getCurrentContext(), this, "initialize");
    }

    
    public int[] sort(int[] array) {
        IRubyObject ruby_array = JavaUtil.convertJavaToRuby(__ruby__, array);
        IRubyObject ruby_result = RuntimeHelpers.invoke(__ruby__.getCurrentContext(), this, "sort", ruby_array);
        return (int[])ruby_result.toJava(int[].class);

    }

    
    public void siftdown(int start, int end_, int[] array) {
        IRubyObject ruby_start = JavaUtil.convertJavaToRuby(__ruby__, start);
        IRubyObject ruby_end_ = JavaUtil.convertJavaToRuby(__ruby__, end_);
        IRubyObject ruby_array = JavaUtil.convertJavaToRuby(__ruby__, array);
        IRubyObject ruby_result = RuntimeHelpers.invoke(__ruby__.getCurrentContext(), this, "siftdown", ruby_start, ruby_end_, ruby_array);
        return;

    }

}
