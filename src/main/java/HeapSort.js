HeapSort = new com.orangescape.jvmlang.Sorter({
  "sort" : function(arr) {
    heap_sort(arr)
  },
  "heap_sort": function (arr) {
    var end, _ref, _results;
    put_array_in_heap_order(arr);
    end = arr.length - 1;
    _results = [];
    while (end > 0) {
      _ref = [arr[end], arr[0]], arr[0] = _ref[0], arr[end] = _ref[1];
      sift_element_down_heap(arr, 0, end);
      _results.push(end -= 1);
    }
    return _results;
  },

  "put_array_in_heap_order": function (arr) {
    var i, _results;
    i = arr.length / 2 - 1;
    i = Math.floor(i);
    _results = [];
    while (i >= 0) {
      sift_element_down_heap(arr, i, arr.length);
      _results.push(i -= 1);
    }
    return _results;
  },

  "sift_element_down_heap": function (heap, i, max) {
    var c1, c2, i_big, _ref;
    while (i < max) {
      i_big = i;
      c1 = 2 * i + 1;
      c2 = c1 + 1;
      if (c1 < max && heap[c1] > heap[i_big]) {
        i_big = c1;
      }
      if (c2 < max && heap[c2] > heap[i_big]) {
        i_big = c2;
      }
      if (i_big === i) {
        return;
      }
      _ref = [heap[i_big], heap[i]], heap[i] = _ref[0], heap[i_big] = _ref[1];
      i = i_big;
    }
  }
});
