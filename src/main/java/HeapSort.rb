require 'java'
java_import 'com.orangescape.jvmlang.Sorter'
java_package 'com.orangescape.jvmlang.ruby'


class HeapSort 
  java_implements 'Sorter'
  java_signature 'public  int[] sort(int[] array)'
  def sort(array)
    # in pseudo-code, heapify only called once, so inline it here
    ((array.length - 2) / 2).downto(0) {|start| siftdown(start, array.length - 1, array)}
 
    # "end" is a ruby keyword
    (array.length - 1).downto(1) do |end_|
      array[end_], array[0] = array[0], array[end_]
      siftdown(0, end_ - 1, array)
    end
    array
  end
  java_signature 'public void siftdown(int start,int end_,int[] array)'
  def siftdown(start, end_, array)
    root = start
    loop do
      child = root * 2 + 1
      break if child > end_
      if child + 1 <= end_ and array[child] < array[child + 1]
        child += 1
      end
      if array[root] < array[child]
        array[root], array[child] = array[child], array[root]
        root = child
      else
        break
      end
    end
  end
end


