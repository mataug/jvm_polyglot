package csv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class Parser {

	public static Integer[] parseIntCSV(String csv) {
		String[] snums = csv.split(",");
		ArrayList<Integer> arr = new ArrayList<Integer>();

		for (String n : snums) {
			int parsed = Integer.parseInt(n);
			arr.add(parsed);
		}

		return (Integer[]) arr.toArray(new Integer[arr.size()]);
	}

	private static String readFile(String path) throws IOException {
		File file = null ;
		FileInputStream stream = null ;
		try {
			file = new File(path);
			stream = new FileInputStream(file);
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		}catch(FileNotFoundException e){
			
			throw new FileNotFoundException(e.getMessage()+":"+file.getAbsolutePath());
		} finally {
			if (stream!=null)stream.close();
		}
	}

	public static int[] unbox(Integer[] arr){

		int[] a = new int[arr.length];
		int count=0;

		for(int i : arr) a[count++] = i;
		return a;
	}
	
	public static Integer[] parseIntCsvFile(String filepath) throws IOException{
		return parseIntCSV(readFile(filepath));
	}


}
